package me.kokoszka.testing;

import me.kokoszka.testing.Oddity;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tomek on 2016-05-31.
 */
public class OddityTester {

    private Oddity sut = new Oddity();


    @Test
    public void shouldBeTrueForOne() throws Exception {
        //given

        Integer one = new Integer(1);


        //when
        boolean actual = sut.isOdd(one);

        //then
        assertTrue(actual);
    }


    @Test
    public void shouldBeFalseForZero() throws Exception {
        //given
        Integer zero = new Integer(0);


        //when
        boolean actual = sut.isOdd(zero);

        //then
        assertFalse(actual);
    }


    @Test
    public void shouldBeFalseForTwo() throws Exception {
        //given
        Integer two = new Integer(2);


        //when
        boolean actual = sut.isOdd(two);

        //then
        assertFalse(actual);
    }


    @Test
    public void shouldBeTrueForMinusThree() throws Exception {
        //given
        Integer twominusThree = new Integer(-3);


        //when
        boolean actual = sut.isOdd(twominusThree);

        //then
        assertTrue(actual);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForNull() throws Exception {
        //given
        Integer nullValue = null;


        //when
        sut.isOdd(nullValue);
    }
}
