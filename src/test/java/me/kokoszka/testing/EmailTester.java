package me.kokoszka.testing;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Tomek on 2016-05-31.
 */
public class EmailTester {

    @Test
    public void shouldBeFalseForNull() {
        // given
        String email = null;
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(email);

        // then
        assertFalse(actual);
    }

    @Test
    public void shouldBeFalseForEmptyString() {
        // given
        String email = "";
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(email);

        // then
        assertFalse(actual);
    }

    @Test
    public void shouldBeFalseForLackAt(){
        // given
        String lackAt = "domain.com";
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(lackAt);

        // then
        assertFalse(actual);
    }

    @Test
    public void shouldBeFalseForLessThenThreeSignsOfName() {
        // given
        String email = "a@wp.pl";
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(email);

        // then
        assertFalse(actual);
    }

    @Test
    public void shouldBeFalseForLessThenOneDotAfterAt() {
        // given
        String email = "a@wppl";
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(email);

        // then
        assertFalse(actual);
    }

    @Test
    public void shouldBeFalseWhenDomainHasOneSign() {
        // given
        String email = "a@a.pl";
        Email sut = new Email();

        // when
        boolean actual = sut.isValid(email);

        // then
        assertFalse(actual);
    }
}
