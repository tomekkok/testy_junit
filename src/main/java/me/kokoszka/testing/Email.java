package me.kokoszka.testing;

/**
 * Created by Tomek on 2016-05-31.
 */
public class Email {

    public boolean isValid(String email){

        if (email == null) {
            return false;
        }
        if (email == "") {
            return false;
        }
        if (email.indexOf('@') == -1  || email.indexOf('@') < 3 ) {
            return false;
        } else {
            String[] parts = email.split("@");
            if (parts[1].indexOf('.') == -1) {
                return false;
            }
            }



        return true;
    }
}
