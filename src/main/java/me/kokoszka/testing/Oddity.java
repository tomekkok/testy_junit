package me.kokoszka.testing;

/**
 * Created by Tomek on 2016-05-31.
 */
public class Oddity {

    public boolean isOdd(Integer value) {

        if (value == null) {
            throw new IllegalArgumentException();
        }


        return value % 2 != 0;

    }
}
